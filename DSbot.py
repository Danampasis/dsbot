import sqlite3
import discord
from discord.ext import commands, tasks
from PIL import Image, ImageDraw, ImageFont, ImageEnhance, ImageFilter, ImageOps
from redminelib import Redmine
import datetime
import praw
import random
import requests
from io import BytesIO
import os
from bs4 import BeautifulSoup
import shutil
import auth

reddit = praw.Reddit(client_id=auth.reddit_client_id,
                     client_secret=auth.reddit_client_secret,
                     user_agent=auth.reddit_user_agent)

subreddit = reddit.subreddit("ProgrammerHumor")

redmine = Redmine('https://projects.directsolutions.io', key=auth.redmine_key)

conn = sqlite3.connect('ds.db')

c = conn.cursor()


client = commands.Bot(command_prefix='!')



@client.command()
async def load(ctx, extension):
    client.load_extension(f'cogs.{extension}')


@client.command()
async def unload(ctx, extension):
    client.unload_extension(f'cogs.{extension}')


@client.command()
async def reload(ctx, extension):
    client.reload_extension(f'cogs.{extension}')

for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')

@client.command()
async def dbins(ctx):
    await client.wait_until_ready()
    guild = client.get_guild(689746415077163011)
    for member in guild.members:
        print("Adding " + member.name + " to the database")
        c.execute("INSERT INTO users(discord_id,joined_at,created_at) VALUES (?,?,?)",
                  (member.id, member.joined_at, member.created_at))
    conn.commit()

# @client.command()
# @commands.guild_only()
# async def issues(ctx):
#     channel = client.get_channel(729585443938762822)
#     for issue in webapp_issues:
#         embed = discord.Embed(title=f"{issue.id}",
#                               colour=discord.Colour(0x32CD32), url=f"https://projects.directsolutions.io/issues/{issue.id}",
#                               description=issue.subject)
#
#         # embed.set_author(name=issue.assignee, url="",
#         #                  icon_url="https://media.licdn.com/dms/image/C4D0BAQENAtJp2-6oMw/company-logo_200_200/0?e=2159024400&v=beta&t=wBjpHGhvj6DT2gLfvKFEZL8v9187UOZ1Bj6u-QrNl_c")
#         # embed.set_footer(text="footer text", icon_url="https://cdn.discordapp.com/embed/avatars/0.png")
#         # embed.add_field(name="Assigned to: ", value=issue.assigned_to_id, inline=True)
#         await channel.send(embed=embed)
#         print(issue)

@client.command()
@commands.guild_only()
async def ping(ctx):
    await ctx.send('pong!')

@client.command(name='maskify')
@commands.guild_only()
@commands.cooldown(1, 30, commands.BucketType.guild)
async def maskify(ctx):
    async with ctx.typing():
        usr_img = requests.get(ctx.message.author.avatar_url)
        img = Image.open(BytesIO(usr_img.content))
        img = img.resize((1024,1024))
        mask = Image.open(random.choice(['assets/gas-mask.png','assets/n951.png','assets/n952.png','assets/surgical.png','assets/cuntosaurus.png','assets/2020.png','assets/beekind.png','assets/twatwaffle.png']))
        img.paste(mask,(0,0),mask)
        img.save('masked.png')
        await ctx.send(file=discord.File('masked.png'))
@maskify.error
async def maskify_error(ctx, error):
    if isinstance(error, commands.CommandOnCooldown):
        msg = 'Είναι σε cooldown, δοκίμασε ξανά σε {:.2f}s'.format(error.retry_after)
        await ctx.send(msg)
    else:
        raise error


@client.command()
async def meme(ctx):
    memes_submissions = reddit.subreddit('ProgrammerHumor').hot()
    post_to_pick = random.randint(1, 10)
    for i in range(0, post_to_pick):
        submission = next(x for x in memes_submissions if not x.stickied)

    await ctx.send(submission.url)

@tasks.loop(minutes=20, reconnect=True)
async def covid_update():
    guild = client.get_guild(689746415077163011)
    print('COVID UPDATE RUNNING')
    channel = client.get_channel(740461721847726111)
    e = open('link.txt', 'r')
    yesterdays_link = e.readline()
    e.close()
    site = requests.get('https://covid19.gov.gr/covid19-live-analytics/')
    soup = BeautifulSoup(site.content, features="html.parser")
    # print(soup)
    img = soup.findAll('img', {"class": "attachment-large size-large"})
    img_src = img[0].get('src')
    if yesterdays_link != img_src:
        print('cases updated!')
        e = open('link.txt', 'w')
        e.write(img_src)
        e.close()
        r = requests.get(img_src, stream=True)

        r.raw.decode_content = True
        with open('newcases.jpg', 'wb') as f:
            shutil.copyfileobj(r.raw, f)
        await channel.send(file=discord.File('newcases.jpg'))
    else:
        print('Same file, cases not updated!')
covid_update.start()

@covid_update.before_loop
async def before_printer(self):
    print('waiting...')
    await client.wait_until_ready()


# @client.event
# async def on_member_join(member):
#     await client.wait_until_ready()
#     channel = client.get_channel(733632829191094353)
#     await channel.send(f'Καλωσόρισες {member.mention} .')

client.run(auth.discord_token)