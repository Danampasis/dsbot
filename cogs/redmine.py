import discord
from discord.ext import commands
from redminelib import Redmine
import sqlite3

class Example(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.redmine = Redmine('https://projects.directsolutions.io', key='09bc2eae80a6ab3c3cc3a4489e5a613a42574f66')
        self.conn = sqlite3.connect('ds.db')
        self.c = self.conn.cursor()

    @commands.Cog.listener()
    async def on_ready(self):
        print('Redmine cog is ONLINE')

    @commands.command()
    async def red(self,ctx):
        redmine = self.redmine
        webapp_issues = redmine.issue.filter(assigned_to_id=18)
        print(list(webapp_issues))
        await ctx.send('BRRRRRR!')

    @commands.command()
    async def myissues(self, ctx):
        async with ctx.typing():
            print(ctx.message.author.id)
            c = self.c
            conn = self.conn
            c.execute("SELECT projects_id FROM users WHERE discord_id =?", (ctx.message.author.id,))
            id = c.fetchone()[0]
            redmine = self.redmine
            issues = redmine.issue.filter(assigned_to_id=id)
            for issue in issues:
                pr_color = discord.Colour(0x00fffb)
                for att in dir(issue):
                    print(att, getattr(issue, att))
                print(issue.priority)
                try:
                    if str(issue.priority) == 'Low':
                        print('low ffffffff')
                        pr_color = discord.Colour(0x00fffb)
                    if str(issue.priority) == 'Normal':
                        pr_color = discord.Colour(0x00ff48)
                    if str(issue.priority) == 'High':
                        pr_color = discord.Colour(0xeeff00)
                    if str(issue.priority) == 'Urgent':
                        pr_color = discord.Colour(0xff6f00)
                    if str(issue.priority) == 'Immediate':
                        pr_color = discord.Colour(0xff0000)
                    else:
                        pass
                except:
                    print('SMTH WENT WRONG!!!!!!')
                    pass
                embed = discord.Embed(title=f"{issue.id}",
                                      colour=pr_color,
                                      url=f"https://projects.directsolutions.io/issues/{issue.id}",
                                      description=issue.subject)
                embed.set_thumbnail(url=ctx.message.author.avatar_url)
                try:
                    embed.add_field(name='Estimated time',value=issue.estimated_hours)
                except:
                    pass
                try:
                    embed.add_field(name='Done %',value=issue.done_ratio)
                except:
                    pass
                try:
                    embed.add_field(name='Start Date',value=issue.start_date)
                except:
                    pass
                try:
                    embed.add_field(name='Due Date',value=issue.due_date)
                except:
                    pass
                await ctx.message.author.send(embed=embed)

            # embed.set_author(name=issue.assignee, url="",
            #                  icon_url="https://media.licdn.com/dms/image/C4D0BAQENAtJp2-6oMw/company-logo_200_200/0?e=2159024400&v=beta&t=wBjpHGhvj6DT2gLfvKFEZL8v9187UOZ1Bj6u-QrNl_c")
            # embed.set_footer(text="footer text", icon_url="https://cdn.discordapp.com/embed/avatars/0.png")
            # embed.add_field(name="Assigned to: ", value=issue.assigned_to_id, inline=True)
            await ctx.send(f'{ctx.message.author.mention} you have {len(issues)} open issues. See your DMs for more info.')



def setup(client):
    client.add_cog(Example(client))
