import discord
from discord.ext import commands
import requests
import random
import json

class Example(commands.Cog):

    def __init__(self, client):
        self.client = client

    # @commands.Cog.listener()
    # async def on_ready(self):
    #     print('BOT IS ONLINE from cogs')

    @commands.command()
    async def xkcd(self,ctx):
        comic = requests.get(f'https://xkcd.com/{random.randrange(1,2355)}/info.0.json')
        await ctx.send(json.loads(comic.content)['img'])
        # await ctx.send(comic.content['img'])


def setup(client):
    client.add_cog(Example(client))
