import discord
from discord.ext import commands

class Example(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('Example cog ONLINE')

    @commands.command()
    async def pong(self,ctx):
        await ctx.send('BRRRRRR!')


def setup(client):
    client.add_cog(Example(client))
