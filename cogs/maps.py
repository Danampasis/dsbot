import discord
from discord.ext import commands
import googlemaps
from datetime import datetime

class Example(commands.Cog):


    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('Google maps cog ONLINE')

    @commands.command()
    async def maps(self,ctx):
        gmaps = googlemaps.Client(key='AIzaSyAw_WFeqS5-UFabQm7cDPaTp5K5sQ1vr7A')

        # Geocoding an address
        geocode_result = gmaps.geocode('1600 Amphitheatre Parkway, Mountain View, CA')

        # Look up an address with reverse geocoding
        reverse_geocode_result = gmaps.reverse_geocode((40.714224, -73.961452))

        # Request directions via public transit
        now = datetime.now()
        directions_result = gmaps.directions("Sydney Town Hall",
                                             "Parramatta, NSW",
                                             mode="transit",
                                             departure_time=now)
        await ctx.send(directions_result)


def setup(client):
    client.add_cog(Example(client))
