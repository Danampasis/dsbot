import discord
from discord.ext import tasks, commands
import datetime

class Example(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.playing.start()

    def cog_unload(self):
        self.playing.cancel()

    # @commands.Cog.listener()
    # async def on_ready(self):
    #

    @tasks.loop(hours=6)
    async def playing(self):
        client = self.client
        today = datetime.date.today()
        future = datetime.date(2020, 11, 30)
        diff = future - today
        game = discord.Game(f'{diff.days} days left')
        await client.change_presence(status=discord.Status.online, activity=game)

    @playing.before_loop
    async def before_playing(self):
        print('waiting...')
        await self.client.wait_until_ready()


def setup(client):
    client.add_cog(Example(client))
