import discord
from discord.ext import commands
import requests
import json

class Example(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('bo_login cog ONLINE')

    @commands.command()
    async def test_login(self,ctx):
        url = "https://backoffice.need4car.com/dev/authenticate?lang=en"

        payload = {'email': 'admin@need4car.gr',
                   'password': 'adminadmin'}
        files = [

        ]
        headers = {}

        response = requests.request("POST", url, headers=headers, data=payload, files=files,verify=False)
        response = str(response.content)[4:17].split('":')
        await ctx.send(f'{response[0]} {response[1]}')
        await ctx.send(json.loads(response.content))


def setup(client):
    client.add_cog(Example(client))
