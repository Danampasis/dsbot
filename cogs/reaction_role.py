import discord
from discord.ext import commands


class Example(commands.Cog):

    def __init__(self, client):
        self.client = client

    def embed_gen(self, title, desc):
        embed = discord.Embed(title=title,
                              colour=discord.Colour(0x7979ff),
                              description=desc)
        return embed

    @commands.command()
    async def init_reacts(self, ctx):
        client = self.client
        channel = client.get_channel(746423140758061137)
        # rules = client.get_channel(352714645615673345)
        # f = open('react_ids.txt')
        # lines = f.readlines()
        # msg1 = await channel.fetch_message(lines[0])
        # msg2 = await channel.fetch_message(lines[1])
        # msg3 = await channel.fetch_message(lines[2])
        # msg4 = await channel.fetch_message(lines[3])
        # msg5 = await channel.fetch_message(lines[4])
        # msg6 = await channel.fetch_message(lines[5])
        # await msg1.delete()
        # await msg2.delete()
        # await msg3.delete()
        # await msg4.delete()
        # await msg5.delete()
        # await msg6.delete()
        client = self.client
        # confirm_msg = await channel.send(f'Διάβασες και συμφωνείς με τα rules στο κανάλι {rules.mention} ? '
        #                                  f'Αν ναι, αντέδρασε για να συμφωνήσεις. Είναι υποχρεωτικό για να έχεις '
        #                                  f'πρόσβαση στα κανάλια.')
        # yes = client.get_emoji(650058880294060043)
        #
        # await confirm_msg.add_reaction(yes)
        platform_msg = await channel.send(embed=Example.embed_gen(self, 'Επιλογή ρόλων project', 'A. Api\nB. Backoffice\nC. WebApp\n'
                                                                                           'D. MOTIF'))
        a = client.get_emoji(748845150624808990)
        b = client.get_emoji(748845150670946354)
        c = client.get_emoji(748845150670946364)
        d = client.get_emoji(748845250407039026)
        await platform_msg.add_reaction(a)
        await platform_msg.add_reaction(b)
        await platform_msg.add_reaction(c)
        await platform_msg.add_reaction(d)
        # lfg_msg = await channel.send(embed=Example.embed_gen(self, 'LFG roles toggle', 'Επιλέξτε lfg casual και ranked '
        #                                                                                'ρόλους αντίστοιχα, σας '
        #                                                                                'επιτρέπουν να κάνετε ping και '
        #                                                                                'να λαμβάνεται ειδοποιήσεις '
        #                                                                                'lfg.'))
        # lfg_casual = client.get_emoji(665870489616384012)
        # lfg_ranked = client.get_emoji(665870458310098964)
        # await lfg_msg.add_reaction(lfg_casual)
        # await lfg_msg.add_reaction(lfg_ranked)
        # news_msg = await channel.send(embed=Example.embed_gen(self, 'News ping toggles', 'Επιλέξτε τι ειδοποιήσεις '
        #                                                                                  'θέλετε να λαμβάνεται '
        #                                                                                  'ανάμεσα σε server news, '
        #                                                                                  'game news, event news.'))
        # server_news = client.get_emoji(670438410313400321)
        # game_news = client.get_emoji(670438293216690176)
        # event_news = client.get_emoji(670438372191371274)
        # await news_msg.add_reaction(server_news)
        # await news_msg.add_reaction(game_news)
        # await news_msg.add_reaction(event_news)
        # quarantine_msg = await channel.send(embed=Example.embed_gen(self, 'Quarantine role toggle', 'Επιλέξτε αν '
        #                                                                                             'ενδιαφέρεστε για το '
        #                                                                                             'παιχνίδι Rainbow Six '
        #                                                                                             'Quarantine. Ξεκλειδώνει '
        #                                                                                             'επιπλέον κανάλια και '
        #                                                                                             'ειδοποιήσεις.'))
        # quarantine = client.get_emoji(638678066939297822)
        # await quarantine_msg.add_reaction(quarantine)
        # break_msg = await channel.send('------------------------------------------------------------------------------')
        # disclaimer_msg = await channel.send('*Με την επιλογή ρόλου πλατφόρμας συμφωνείτε στην καταγραφή και '
        #                                     'επεξεργασία ανωνυμοποιημένων δεδομένων από το Discord και Rainbow Six τα '
        #                                     'οποία βοηθούν στην δημιουργία καλύτερων εμπειριών για εσάς. Μπορείτε να '
        #                                     'ζητήσετε να δείτε όλα τα δεδομένα που έχουν καταγραφεί.*')
        with open('react_ids.txt', 'w') as f:
            # f.write(str(confirm_msg.id)+'\n')
            f.write(str(platform_msg.id) + '\n')
            # f.write(str(lfg_msg.id) + '\n')
            # f.write(str(news_msg.id) + '\n')
            # f.write(str(quarantine_msg.id) + '\n')
            # f.write(str(break_msg.id) + '\n')
            # f.write(str(disclaimer_msg.id))

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        # confirm_msg = Example.init_reacts(self)
        message_id = payload.message_id
        f = open('react_ids.txt')
        lines = f.readlines()
        if message_id == int(lines[0]):
            print('Platform reaction added')
            print(payload.emoji.name)
            guild = self.client.get_guild(689746415077163011)
            # role = discord.utils.get(guild.roles, name='')
            if payload.emoji.name == 'a_':
                print('one pressed')
                role = discord.utils.get(guild.roles, name='api')
                member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                if role in member.roles:
                    pass
                    print('user has that role')
                else:
                    await member.add_roles(role)
            if payload.emoji.name == 'b_':
                print('two pressed')
                role = discord.utils.get(guild.roles, name='backoffice')
                member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                if role in member.roles:
                    pass
                    print('user has that role')
                else:
                    await member.add_roles(role)
            if payload.emoji.name == 'c_':
                print('three pressed')
                role = discord.utils.get(guild.roles, name='webapp')
                member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                if role in member.roles:
                    pass
                    print('user has that role')
                else:
                    await member.add_roles(role)
            if payload.emoji.name == 'd_':
                print('four pressed')
                role = discord.utils.get(guild.roles, name='motif')
                member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                if role in member.roles:
                    pass
                    print('user has that role')
                else:
                    await member.add_roles(role)
        # if message_id == int(lines[1]):
        #     print('LFG reaction added')
        #     guild = self.client.get_guild(258324654710718467)
        #     # role = discord.utils.get(guild.roles, name='')
        #     if payload.emoji.name == 'lfgcasual':
        #         role = discord.utils.get(guild.roles, name='lfg_casual')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             pass
        #             print('user has that role')
        #         else:
        #             await member.add_roles(role)
        #     if payload.emoji.name == 'lfgranked':
        #         role = discord.utils.get(guild.roles, name='lfg_ranked')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             pass
        #             print('user has that role')
        #         else:
        #             await member.add_roles(role)
        # if message_id == int(lines[2]):
        #     print('News reaction added')
        #     guild = self.client.get_guild(258324654710718467)
        #     # role = discord.utils.get(guild.roles, name='')
        #     if payload.emoji.name == 'servernews':
        #         role = discord.utils.get(guild.roles, name='server_news')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             pass
        #             print('user has that role')
        #         else:
        #             await member.add_roles(role)
        #     if payload.emoji.name == 'gamenews':
        #         role = discord.utils.get(guild.roles, name='r6news')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             pass
        #             print('user has that role')
        #         else:
        #             await member.add_roles(role)
        #     if payload.emoji.name == 'eventnews':
        #         role = discord.utils.get(guild.roles, name='event_news')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             pass
        #             print('user has that role')
        #         else:
        #             await member.add_roles(role)
        # if message_id == int(lines[3]):
        #     print('Quarantine reaction added')
        #     guild = self.client.get_guild(258324654710718467)
        #     # role = discord.utils.get(guild.roles, name='')
        #     if payload.emoji.name == 'quarantine':
        #         role = discord.utils.get(guild.roles, name='Quarantine')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             pass
        #             print('user has that role')
        #         else:
        #             await member.add_roles(role)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        # confirm_msg = Example.init_reacts(self)
        message_id = payload.message_id
        f = open('react_ids.txt')
        lines = f.readlines()
        if message_id == int(lines[0]):
            print('Platform reaction added')
            guild = self.client.get_guild(689746415077163011)
            # role = discord.utils.get(guild.roles, name='')
            if payload.emoji.name == 'a_':
                role = discord.utils.get(guild.roles, name='api')
                member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                if role in member.roles:
                    await member.remove_roles(role)
                    # await member.remove_roles(role)
            if payload.emoji.name == 'b_':
                role = discord.utils.get(guild.roles, name='backoffice')
                member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                if role in member.roles:
                    await member.remove_roles(role)
            if payload.emoji.name == 'c_':
                role = discord.utils.get(guild.roles, name='webapp')
                member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                if role in member.roles:
                    await member.remove_roles(role)
            if payload.emoji.name == 'd_':
                role = discord.utils.get(guild.roles, name='motif')
                member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                if role in member.roles:
                    await member.remove_roles(role)
        # if message_id == int(lines[1]):
        #     print('LFG reaction added')
        #     guild = self.client.get_guild(258324654710718467)
        #     # role = discord.utils.get(guild.roles, name='')
        #     if payload.emoji.name == 'lfgcasual':
        #         role = discord.utils.get(guild.roles, name='lfg_casual')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             await member.remove_roles(role)
        #     if payload.emoji.name == 'lfgranked':
        #         role = discord.utils.get(guild.roles, name='lfg_ranked')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             await member.remove_roles(role)
        # if message_id == int(lines[2]):
        #     print('News reaction added')
        #     guild = self.client.get_guild(258324654710718467)
        #     # role = discord.utils.get(guild.roles, name='')
        #     if payload.emoji.name == 'servernews':
        #         role = discord.utils.get(guild.roles, name='server_news')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             await member.remove_roles(role)
        #     if payload.emoji.name == 'gamenews':
        #         role = discord.utils.get(guild.roles, name='r6news')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             await member.remove_roles(role)
        #     if payload.emoji.name == 'eventnews':
        #         role = discord.utils.get(guild.roles, name='event_news')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             await member.remove_roles(role)
        # if message_id == int(lines[3]):
        #     print('Quarantine reaction added')
        #     guild = self.client.get_guild(258324654710718467)
        #     # role = discord.utils.get(guild.roles, name='')
        #     if payload.emoji.name == 'quarantine':
        #         role = discord.utils.get(guild.roles, name='Quarantine')
        #         member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
        #         if role in member.roles:
        #             await member.remove_roles(role)


def setup(client):
    client.add_cog(Example(client))
